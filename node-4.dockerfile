FROM node:4
ADD ./package.json /wlsjs/package.json
WORKDIR /wlsjs
RUN npm install
ADD . /wlsjs
RUN npm test
