var wlsjs = require('../lib');

wlsjs.api.getAccountCount(function(err, result) {
	console.log(err, result);
});

wlsjs.api.getAccounts(['dan'], function(err, result) {
	console.log(err, result);
	var reputation = wlsjs.formatter.reputation(result[0].reputation);
	console.log(reputation);
});

wlsjs.api.getState('trending/wls', function(err, result) {
	console.log(err, result);
});

wlsjs.api.getFollowing('ned', 0, 'blog', 10, function(err, result) {
	console.log(err, result);
});

wlsjs.api.getFollowers('dan', 0, 'blog', 10, function(err, result) {
	console.log(err, result);
});

wlsjs.api.streamOperations(function(err, result) {
	console.log(err, result);
});

wlsjs.api.getDiscussionsByActive({
  limit: 10,
  start_author: 'thecastle',
  start_permlink: 'this-week-in-level-design-1-22-2017'
}, function(err, result) {
	console.log(err, result);
});
